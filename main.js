var huffman = null;

function btnrun_click(){
	var decoded = document.getElementById('txtin').value;
	var encoded = encode(decoded);
	var ratio   = Math.round((1 - (encoded.length / decoded.length)) * 100) / 100;
	alert('Taux de compréssion : ' + (ratio * 100) + '%');
}

function btnencode_click(){
	document.getElementById('txtout').value = encode(document.getElementById('txtin').value);
}

function btndecode_click(){
	document.getElementById('txtin').value = "DECODE : \n\n" + decode(document.getElementById('txtout').value);
}

function encode(str){
	huffman = Huffman.treeFromArray(str.split(''));
	return huffman.encodeArray(str.split(''));
}
function decode(str){
	return huffman.decode(str)
}


